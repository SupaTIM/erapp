﻿using UnityEngine;
using System.Collections;

public class CameraPicture : MonoBehaviour {
	public WebCamTexture webcamTexture;
	public Color32[] data;

	// Use this for initialization
	void Start () {
		//set up the webcam texture
		webcamTexture = new WebCamTexture();

		// hope for square aspect ratio?
		webcamTexture.requestedWidth = 1000;
		webcamTexture.requestedHeight = 1000;

		webcamTexture.Play();
		
		// orient the camera texture correctly
		if (webcamTexture.videoRotationAngle != 0) {
			transform.eulerAngles = new Vector3 (0, 0, -90);
		}
		
		data = new Color32[webcamTexture.width * webcamTexture.height];	
		
		//set the objects texture to webcam texture
		renderer.material.mainTexture = webcamTexture;

	}
	public byte[] TakeSnapshot()
	{
		Texture2D snap = new Texture2D(webcamTexture.width, webcamTexture.height);
		snap.SetPixels(webcamTexture.GetPixels());
		snap.Apply();
		return snap.EncodeToPNG ();
	}
	// Update is called once per frame
	void Update () {
		//don't update unless the previous frame has been changed. (camera render frames much slower than the update function)
		if (webcamTexture.didUpdateThisFrame)
		{
			webcamTexture.GetPixels32 (data);
			// Do processing of data here.
		}
	}
	
	//should take a screenshot of the current screen view. (for sending to parse)
	public void TakePhoto() {
		Debug.Log(WebCamTexture.devices[0]);
	}
}
