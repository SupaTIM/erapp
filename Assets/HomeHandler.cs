﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

public class HomeHandler : MonoBehaviour {
	public UILabel user;
	public GameObject reportfeed;
	public UIGrid reportfeedUIGrid;
	public GameObject expenserow;
	public IEnumerable<ParseObject> results;
	
	public bool expenseDataReceived;
	
	public void GetExpenses()
	{

		Debug.Log ("Getting Expenses");
		var query = ParseObject.GetQuery (ParseUser.CurrentUser.Get<string>("company")).
			WhereEqualTo ("user", ParseUser.CurrentUser.Username);
		query.FindAsync ().ContinueWith(t =>
		                                {
			if(t.IsFaulted || t.IsCanceled) {
				Debug.Log("Error getting expenses");
			}
			else if(t.IsCompleted) {
				Debug.Log("Successfully grabbed records for company " + 
				          ParseUser.CurrentUser.Get<string>("company") + 
				          " and user " + ParseUser.CurrentUser.Username);
				results = t.Result;
				expenseDataReceived = true;
			}
		});
		
	}
	
	public void CreateRow()
	{
		GameObject instance;
		int index = 1;
		Expenserowhandler erhandler;
		UILabel[] labels;

		foreach(ParseObject o in results){
			Debug.Log (o.Get<string>("name"));
			Debug.Log ("creating expense");
			Expense e = new Expense();
			e.name = o.Get<string>("name");
			e.id = o.ObjectId;
			e.date = System.String.Format("MM/dd/yyyy",o.Get<System.DateTime>("date"));
			e.cost = o.Get<string>("amount");
			// ROW CREATION
			Debug.Log("making a row " + index);
			instance = Instantiate(expenserow, Vector3.zero, Quaternion.identity) as GameObject;
			erhandler = instance.GetComponent("Expenserowhandler") as Expenserowhandler;
			erhandler.amount.text = "$" + e.cost;
			erhandler.name.text = "N: " + e.name;
			erhandler.date.text = "D: " + e.date;
			
			labels = NGUITools.AddChild (reportfeed, instance).transform.GetComponentsInChildren<UILabel>();
			index++;
		}
		reportfeedUIGrid.repositionNow = true;
	}
	public void GetUserExpenses()
	{
		Debug.Log ("Getting Expenses");
		var query = ParseObject.GetQuery (ParseUser.CurrentUser.Get<string>("company")).
			WhereEqualTo ("user", user.text);
		query.FindAsync ().ContinueWith(t =>
		                                {
			if(t.IsFaulted || t.IsCanceled)
				Debug.Log("Error getting epxenses");
			else if(t.IsCompleted) {
				Debug.Log("Successfully grabbed records for company " + 
				          ParseUser.CurrentUser.Get<string>("company") + 
				          " and user " + ParseUser.CurrentUser.Username);
				IEnumerable<ParseObject> results = t.Result;
				foreach(ParseObject o in results)
				{
					UILabel[] labels = NGUITools.AddChild (reportfeed, expenserow).transform.GetComponentsInChildren<UILabel>();
					labels [0].text = o.Get<string>("date");
					labels [1].text = o.Get<string>("name");
					labels [2].text = o.Get<string>("amount");
					Debug.Log (o.Get<string>("name"));
				}
			}
		});
	}
	
	public List<string> getUsers()
	{
		List<string> users = new List<string> ();
		Debug.Log ("getting users for company: " + ParseUser.CurrentUser.Get<string>("company"));
		var query = ParseObject.GetQuery ("company").WhereEqualTo ("company",ParseUser.CurrentUser.Get<string> ("company"));
		query.FindAsync ().ContinueWith (t =>
		                                 {
			if (t.IsFaulted || t.IsCanceled)
				Debug.Log ("Error Getting employees");
			else if(t.IsCompleted){
				Debug.Log ("completed getting users, parsing");
				foreach (ParseObject o in t.Result)
					if (!users.Contains (o.Get<string> ("user")))
						users.Add (o.Get<string> ("user"));
				Debug.Log("Finished getting users");
				foreach (string s in users)
					Debug.Log ("Users: " + s);
				
			}
		});
		Debug.Log ("returning from get users");
		return users;
	}
	public void Awake ()
	{
		reportfeedUIGrid = reportfeed.GetComponent("UIGrid") as UIGrid;
		GetExpenses ();
		Debug.Log ("in home handler");
		getUsers ();
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (expenseDataReceived) {
			CreateRow();
			expenseDataReceived = false;
		}
	}
	
	class ExpenseRow {
		UILabel name;
		UILabel date;
		UILabel amount;
		UILabel type;
		UILabel id;
		UIButton singleView;
	}
}
