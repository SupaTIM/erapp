﻿using UnityEngine;
using System.Collections;

public class DateRoller : MonoBehaviour {
	public UILabel monthLabel;
	public UILabel dayLabel;
	public UILabel yearLabel;
	
	public int monthInt;
	public int dayInt;
	public int yearInt;
	
	// Use this for initialization
	void Start () {
		monthInt = System.DateTime.Now.Month;
		dayInt = System.DateTime.Now.Day;
		yearInt = System.DateTime.Now.Year;
		UpdateLabel ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void IncrementMonth () {
		monthInt++;
		if (monthInt == 13) monthInt = 1;
		UpdateLabel ();
		
	}
	
	public void DecerementMonth () {
		monthInt--;
		if (monthInt == 0) monthInt = 12;
		UpdateLabel ();
	}
	
	public void IncrementDay () {
		dayInt++;
		if (dayInt > System.DateTime.DaysInMonth(yearInt, monthInt)) dayInt = 1;
		UpdateLabel ();
	}	
	
	public void DecerementDay () {
		dayInt--;
		if (dayInt == 0) dayInt = System.DateTime.DaysInMonth(yearInt, monthInt);
		UpdateLabel ();
	}
	
	public void IncrementYear () {
		yearInt++;
		UpdateLabel ();
	}
	
	public void DecrementYear () {
		yearInt--;
		UpdateLabel ();
	}
	
	void UpdateLabel () {
		if (dayInt > System.DateTime.DaysInMonth(yearInt, monthInt)) dayInt = System.DateTime.DaysInMonth(yearInt, monthInt);
		
		monthLabel.text = monthInt.ToString();
		dayLabel.text = dayInt.ToString();
		yearLabel.text = yearInt.ToString();
	}

}
