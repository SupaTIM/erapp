﻿using UnityEngine;
using System.Collections;

public class LoadPanel : MonoBehaviour {
	
	public GameObject[] panelsToClose;
	public GameObject[] panelToActivate;
	public LoginHandler loginHandler;
	
	public GameObject noConnection;

	public void ActivatePanel () {
		//fail connection
		if (loginHandler.ping("4.2.2.2") < 0) {
			NGUITools.SetActive(noConnection, true);
			return;
		}
		
		//has connection
		NGUITools.SetActive(noConnection, false);
		foreach (GameObject c in panelsToClose) {
			NGUITools.SetActive(c, false);
		}
		foreach (GameObject a in panelToActivate) {
			NGUITools.SetActive(a, true);
		}
	}
}
