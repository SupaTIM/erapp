﻿using UnityEngine;
using System.Collections;
using Parse;
public class CreateHandler : MonoBehaviour {
	public UIInput amount;
	//public UIInput date;
	public UIInput type;
	public UIInput name;
	
	public UILabel mo;
	public UILabel da;
	public UILabel ye;
	
	public Texture2D image;
	public void submitExpense()
	{
		if (ParseUser.CurrentUser == null)
						Debug.Log ("User is null");
		else 
			Debug.Log("User should be logged in: " + ParseUser.CurrentUser.Get<string>("company"));
		Debug.Log ("Expense info: " + name.label.text);
		ParseObject o = new ParseObject(ParseUser.CurrentUser.Get<string>("company"));
		o ["name"] = name.text;
		o ["date"] = System.DateTime.ParseExact("MM/dd/yyyy", mo.text + "/" + da.text + "/" + ye.text, null);
		o ["amount"] = amount.text;
		o ["user"] = ParseUser.CurrentUser.Username;
		o.SaveAsync ().ContinueWith (t => {
			if(t.IsFaulted || t.IsCanceled)
				Debug.Log ("Error saving: " + t.Exception);
			else
				Debug.Log("Expense Saved");
		});
	}

	public void submitExpenseWithImage()
	{

		if (ParseUser.CurrentUser == null)
			Debug.Log ("User is null");
		else 
			Debug.Log("User should be logged in: " + ParseUser.CurrentUser.Get<string>("company"));
		Debug.Log ("Expense info: " + name.label.text);
		ParseObject o = new ParseObject(ParseUser.CurrentUser.Get<string>("company"));
		o ["name"] = name.text;
		o ["date"] = mo.text + "/" + da.text + "/" + ye.text;
		o ["amount"] = amount.text;
		o ["user"] = ParseUser.CurrentUser.Username;
		o ["image"] = image.EncodeToPNG ();
		o.SaveAsync ().ContinueWith (t => {
			if(t.IsFaulted || t.IsCanceled)
				Debug.Log ("Error saving: " + t.Exception);
			else
				Debug.Log("Expense Saved");
		});
	}
	// Use this for initialization
	void Start () {
		//date.text = System.DateTime.Today.ToShortDateString ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
