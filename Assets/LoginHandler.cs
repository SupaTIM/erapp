﻿using UnityEngine;
using System.Collections;
using System.Threading;
using Parse;
using System.Threading.Tasks;

public class LoginHandler : MonoBehaviour {
	public UILabel username;
	public UIInput password;
	public UIInput rconfirmpass;
	public UILabel rcompany;
	
	public LoadPanel loadHome;
	public LoadPanel loadLogin;
	public GameObject loginFailMessage;
	public UILabel loginFailText;
	
	static bool logInFailed;
	static bool completeLogTransition;
	
	static string errorCode;
	
	public HomeHandler homeHandler;
	
	// Use this for initialization
	void Start () {
		if (ParseUser.CurrentUser != null && ParseUser.CurrentUser.IsAuthenticated) {
			completeLogTransition = true;
			Debug.Log("Auto-logging in as user: " + ParseUser.CurrentUser.Username);
		}
	}
	public void Login()
	{
		CheckConnection();
		Debug.Log ("Logging in");
		Debug.Log("Value of login: ");
		Debug.Log ("Password: " + password.value);
		ParseUser.LogOut();
		ParseUser.LogInAsync (username.text, password.text).ContinueWith (t => {
						if (t.IsFaulted || t.IsCanceled) {
								logInFailed = true;
								errorCode = "Failed to Login";
								Debug.Log ("Failed to login: " + t.Exception);
						}
						else {
								logInFailed = false;
								completeLogTransition = true;
								Debug.Log ("Login successful");
						}
				});
	}
	public void LogOut() 
	{
		Debug.Log ("Logging out");
		ParseUser.LogOut();
		loadLogin.ActivatePanel();
	}
	public void Register()
	{
		CheckConnection();
		var user = new ParseUser()
		{
			Username = username.text,
			Password = password.value,
			Email = username.text
		};
		if (rconfirmpass.value != password.value) {
						logInFailed = true;
						errorCode = "Mismatched Password";
						return;
				}
		if (rcompany.text.Length < 1) // use first part of usernbame and email host for company
						user ["company"] = username.text.Split ('@')[0] + username.text.Split('@')[1].Split('.')[0];
		else
			user ["company"] = rcompany.text;

		Task signUpTask = user.SignUpAsync().ContinueWith(t => {
			if(t.IsFaulted || t.IsCanceled)
			foreach(var e in t.Exception.InnerExceptions) {
				ParseException parseException = (ParseException) e;
				Debug.Log("Error message " + parseException.Message);
				Debug.Log("Error code: " + parseException.Code);
				errorCode = "Error message " + parseException.Message;
				logInFailed = true;
				
			} else {
				addToCompany(rcompany.text, username.text);
				completeLogTransition = true;
				Debug.Log ("register success");
				logInFailed = false;
			}
		});
	}
	private void CheckConnection() 
	{
		if (ping ("4.2.2.2") == -1) {
			logInFailed = true;
			errorCode = "Bad internet connection";
			Debug.Log ("Bad internet connection");
		}
	}
	private void addToCompany(string company, string username)
	{
		ParseObject o = new ParseObject ("company");
		o ["user"] = username;
		o ["company"] = company;
		o.SaveAsync ();
	}
	public int ping(string ip)
	{
		Ping p = new Ping (ip);
		while (!p.isDone)
				Thread.Sleep (10);
		return p.time;

	}
	
	// Update is called once per frame
	void Update () {
		if (logInFailed && loginFailText.text != errorCode) {
			loginFailText.text = errorCode;
			NGUITools.SetActive(loginFailMessage, logInFailed);
		}
		if (completeLogTransition) {
			NGUITools.SetActive(loginFailMessage, logInFailed);
			loadHome.ActivatePanel();
			completeLogTransition = false;
		}
	}
}
