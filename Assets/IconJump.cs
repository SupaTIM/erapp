﻿using UnityEngine;
using System.Collections;

public class IconJump : MonoBehaviour {
	public Transform[] path;

	// Use this for initialization
	void Start () {
		StartCoroutine(WaitJump());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator WaitJump() {
		yield return new WaitForSeconds(1.5f);
		iTween.MoveTo(gameObject, iTween.Hash(
			"path", 	path,
			"time",		0.25f,
			"speed",	2f
			));
	}
}
